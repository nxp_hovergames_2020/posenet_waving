# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import collections
from functools import partial
import re
import time

import numpy as np
from PIL import Image
import svgwrite
import gstreamer

from pose_engine import PoseEngine
from pose_engine import KeypointType

EDGES = (
    (KeypointType.NOSE, KeypointType.LEFT_EYE),
    (KeypointType.NOSE, KeypointType.RIGHT_EYE),
    (KeypointType.NOSE, KeypointType.LEFT_EAR),
    (KeypointType.NOSE, KeypointType.RIGHT_EAR),
    (KeypointType.LEFT_EAR, KeypointType.LEFT_EYE),
    (KeypointType.RIGHT_EAR, KeypointType.RIGHT_EYE),
    (KeypointType.LEFT_EYE, KeypointType.RIGHT_EYE),
    (KeypointType.LEFT_SHOULDER, KeypointType.RIGHT_SHOULDER),
    (KeypointType.LEFT_SHOULDER, KeypointType.LEFT_ELBOW),
    (KeypointType.LEFT_SHOULDER, KeypointType.LEFT_HIP),
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_ELBOW),
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_HIP),
    (KeypointType.LEFT_ELBOW, KeypointType.LEFT_WRIST),
    (KeypointType.RIGHT_ELBOW, KeypointType.RIGHT_WRIST),
    (KeypointType.LEFT_HIP, KeypointType.RIGHT_HIP),
    (KeypointType.LEFT_HIP, KeypointType.LEFT_KNEE),
    (KeypointType.RIGHT_HIP, KeypointType.RIGHT_KNEE),
    (KeypointType.LEFT_KNEE, KeypointType.LEFT_ANKLE),
    (KeypointType.RIGHT_KNEE, KeypointType.RIGHT_ANKLE),
    
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_WRIST),
    (KeypointType.LEFT_SHOULDER,  KeypointType.LEFT_WRIST),
)

EDGES_HIGHLIGHT_LEFT = (
    (KeypointType.LEFT_SHOULDER, KeypointType.LEFT_HIP),
    (KeypointType.LEFT_SHOULDER,  KeypointType.LEFT_WRIST),
)

EDGES_HIGHLIGHT_RIGHT = (
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_HIP),
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_WRIST),
)


def shadow_text(dwg, x, y, text, font_size=16):
    dwg.add(dwg.text(text, insert=(x + 1, y + 1), fill='black',
                     font_size=font_size, style='font-family:sans-serif'))
    dwg.add(dwg.text(text, insert=(x, y), fill='white',
                     font_size=font_size, style='font-family:sans-serif'))


def draw_pose(dwg, pose, src_size, inference_box, color='yellow', threshold=0.2, hands=[False, False]):
    box_x, box_y, box_w, box_h = inference_box
    scale_x, scale_y = src_size[0] / box_w, src_size[1] / box_h
    xys = {}
    for label, keypoint in pose.keypoints.items():
        if keypoint.score < threshold: continue
        # Offset and scale to source coordinate space.
        kp_x = int((keypoint.point[0] - box_x) * scale_x)
        kp_y = int((keypoint.point[1] - box_y) * scale_y)

        xys[label] = (kp_x, kp_y)
        dwg.add(dwg.circle(center=(int(kp_x), int(kp_y)), r=5,
                           fill='cyan', fill_opacity=keypoint.score, stroke=color))

    for a, b in EDGES:
        if a not in xys or b not in xys: continue
        ax, ay = xys[a]
        bx, by = xys[b]
        dwg.add(dwg.line(start=(ax, ay), end=(bx, by), stroke=color, stroke_width=2))
    
    # left hand
    colour = angle2colour(hands[0])
    for a, b in EDGES_HIGHLIGHT_LEFT:
        if a not in xys or b not in xys: continue
        ax, ay = xys[a]
        bx, by = xys[b]
        dwg.add(dwg.line(start=(ax, ay), end=(bx, by), stroke=colour, stroke_width=3))
    # right hand
    colour = angle2colour(hands[1])
    for a, b in EDGES_HIGHLIGHT_RIGHT:
        if a not in xys or b not in xys: continue
        ax, ay = xys[a]
        bx, by = xys[b]
        dwg.add(dwg.line(start=(ax, ay), end=(bx, by), stroke=colour, stroke_width=3))

def print_pose(pose):
    if pose.score < 0.4: return [None,None]
#    print('\nPose Score: ', pose.score)
    for label, keypoint in pose.keypoints.items():
#        print('  %-20s x=%-4d y=%-4d score=%.1f' %
#              (label.name, keypoint.point[0], keypoint.point[1], keypoint.score))
#        if KeypointType.LEFT_HIP == label: print(keypoint)
#        if KeypointType.LEFT_WRIST == label: print(keypoint)
#        if KeypointType.LEFT_SHOULDER == label: print(keypoint)
        # extract key points values
        if KeypointType.LEFT_HIP == label:       left_p0 = keypoint.point
        if KeypointType.LEFT_SHOULDER == label:  left_p1 = keypoint.point
        if KeypointType.LEFT_WRIST == label:     left_p2 = keypoint.point
        if KeypointType.RIGHT_HIP == label:      right_p0 = keypoint.point
        if KeypointType.RIGHT_SHOULDER == label: right_p1 = keypoint.point
        if KeypointType.RIGHT_WRIST == label:    right_p2 = keypoint.point
    
    # obtain angles
    left_angle  = calculate_angle(left_p0,left_p1,left_p2)    
    right_angle = calculate_angle(right_p0,right_p1,right_p2)    

    left  = detect_hand_waving(left_angle)
    right = detect_hand_waving(right_angle)
    
    left_txt = angle2text(left)
    right_txt = angle2text(right)
    print('Left: %-4s (%-4d)  Right: %-4s (%-4d) ' % (left_txt, left_angle, right_txt, right_angle))
    return [left, right]

def angle2text(angle):
    return "UP" if angle == True else "DOWN"

def angle2colour(angle):
    return 'red' if angle == True else 'blue'
    
def detect_hand_waving(angle):
    if angle > -180 and angle < -90: return True
    if angle > 90 and angle < 180: return True
    return False

def calculate_angle(p0,p1,p2):
    ''' 
    compute angle (in degrees) for p0p1p2 corner
    Inputs:
        p0,p1,p2 - points in the form of [x,y]
    '''
    v0 = np.array(p0) - np.array(p1)
    v1 = np.array(p2) - np.array(p1)
    angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
    angle_deg = np.degrees(angle)
    #print (angle_deg)
    return angle_deg

def avg_fps_counter(window_size):
    window = collections.deque(maxlen=window_size)
    prev = time.monotonic()
    yield 0.0  # First fps value.

    while True:
        curr = time.monotonic()
        window.append(curr - prev)
        prev = curr
        yield len(window) / sum(window)


def run(inf_callback, render_callback):
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--mirror', help='flip video horizontally', action='store_true')
    parser.add_argument('--model', help='.tflite model path.', required=False)
    parser.add_argument('--res', help='Resolution', default='640x480',
                        choices=['480x360', '640x480', '1280x720'])
    parser.add_argument('--videosrc', help='Which video source to use', default='/dev/video0')
    parser.add_argument('--h264', help='Use video/x-h264 input', action='store_true')
    parser.add_argument('--jpeg', help='Use image/jpeg input', action='store_true')
    args = parser.parse_args()

    default_model = 'models/mobilenet/posenet_mobilenet_v1_075_%d_%d_quant_decoder_edgetpu.tflite'
    if args.res == '480x360':
        src_size = (640, 480)
        appsink_size = (480, 360)
        model = args.model or default_model % (353, 481)
    elif args.res == '640x480':
        src_size = (640, 480)
        appsink_size = (640, 480)
        model = args.model or default_model % (481, 641)
    elif args.res == '1280x720':
        src_size = (1280, 720)
        appsink_size = (1280, 720)
        model = args.model or default_model % (721, 1281)

    print('Loading model: ', model)
    engine = PoseEngine(model)
    input_shape = engine.get_input_tensor_shape()
    inference_size = (input_shape[2], input_shape[1])

    gstreamer.run_pipeline(partial(inf_callback, engine), partial(render_callback, engine),
                           src_size, inference_size,
                           mirror=args.mirror,
                           videosrc=args.videosrc,
                           h264=args.h264,
                           jpeg=args.jpeg
                           )


def main():
    n = 0
    sum_process_time = 0
    sum_inference_time = 0
    ctr = 0
    fps_counter = avg_fps_counter(30)

    def run_inference(engine, input_tensor):
        return engine.run_inference(input_tensor)

    def render_overlay(engine, output, src_size, inference_box):
        nonlocal n, sum_process_time, sum_inference_time, fps_counter

        svg_canvas = svgwrite.Drawing('', size=src_size)
        start_time = time.monotonic()
        outputs, inference_time = engine.ParseOutput()
        end_time = time.monotonic()
        n += 1
        sum_process_time += 1000 * (end_time - start_time)
        sum_inference_time += inference_time * 1000

        avg_inference_time = sum_inference_time / n
        

        #shadow_text(svg_canvas, 10, 20, text_line)
        wavingsCount = 0
        for pose in outputs:
            hands = print_pose(pose)
            wavingsCount = wavingsCount+1 if countHands(hands) else wavingsCount
            draw_pose(svg_canvas, pose, src_size, inference_box, 'yellow', 0.2, hands)
    
        text_line = 'PoseNet: %.1fms (%.2f fps) TrueFPS: %.2f Nposes %d NWavesCount %d' % (
            avg_inference_time, 1000 / avg_inference_time, next(fps_counter), len(outputs), wavingsCount
        )
        #print(text_line)
        shadow_text(svg_canvas, 10, 20, text_line)
        return (svg_canvas.tostring(), False)

    run(run_inference, render_overlay)

def countHands(hands): 
    return True if hands[0] == True or hands[1] == True else False

if __name__ == '__main__':
    main()
